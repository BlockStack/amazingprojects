package com.gitlab.blockstack.beekeeper;

import com.gitlab.blockstack.beekeeper.commands.AdminCommandHandler;
import com.gitlab.blockstack.beekeeper.commands.CommandHandler;
import com.gitlab.blockstack.beekeeper.listeners.EventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * beekeeper Plugin
 *
 * @author ${author}
 */
public class Beekeeper extends JavaPlugin
{
    private static Beekeeper self;

    /**
     * Retrieve the plugin instance.
     * @return The Beekeeper instance.
     */
    public static Beekeeper self() {
        return self;
    }

    @Override
	public void onEnable() {
		getCommand("beekeeper").setExecutor(new CommandHandler());
		getCommand("beekeeperadmin").setExecutor(new AdminCommandHandler());
		getServer().getPluginManager().registerEvents(new EventListener(), this);
        self = this;
        this.getLogger().info("beekeeper enabled.");
	}

	@Override
	public void onDisable() {
        self = null;
        this.getLogger().info("{beekeeper} disabled.");
    }

    /**
	 * Retrieve primed gson instance.
	 * 
	 * @param pretty Enable pretty print.
	 * @return GSON instance.
	 */
	public static Gson getGson(boolean pretty) {
		GsonBuilder builder = new GsonBuilder();4
		if (pretty) {
			builder.setPrettyPrinting();
		}
		return builder.create();
	}
}

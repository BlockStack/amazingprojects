package com.gitlab.blockstack.BlockStackFurniture.listeners;

import com.gitlab.blockstack.BlockStackFurniture.BlockStackFurniture;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import de.Ste3et_C0st.FurnitureLib.Events.FurnitureBlockBreakEvent;
import de.Ste3et_C0st.FurnitureLib.Events.FurnitureBlockClickEvent;
import de.Ste3et_C0st.FurnitureLib.Events.FurnitureBreakEvent;
import de.Ste3et_C0st.FurnitureLib.Events.FurnitureClickEvent;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureHelper;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.Type.SQLAction;
import de.Ste3et_C0st.FurnitureLib.main.entity.fEntity;

public class CRusticWalllampLarge extends FurnitureHelper implements Listener{
	
	Location light;
	Vector loc2, loc3;
	boolean redstone = false;

	public CRusticWalllampLarge(ObjectID id){
		super(id);
		//setBlock();
		this.loc2 = id.getStartLocation().toVector();
		this.loc3 = id.getStartLocation().getBlock().getRelative(BlockFace.DOWN).getLocation().toVector();
		this.light = getLutil().getRelativ(getLocation(), getBlockFace(), -1D, 0D);
		Bukkit.getPluginManager().registerEvents(this, BlockStackFurniture.self());
	}
	
	// @Override
	// public void spawn(Location arg0) {
	// 	super.spawn(arg0);
	// 	Bukkit.getPluginManager().registerEvents(this, BlockStackFurniture.self());
	// }
	// private void setBlock(){
	// 	List<Block> blockLocation = new ArrayList<Block>();
	// 	Location location = getLocation().getBlock().getLocation();
	// 	location.setY(location.getY()-1);
	// 	for(int i = 0; i<=3;i++){
	// 		location.setY(location.getY()+1);
	// 		Block block = location.getBlock();
	// 		block.setType(Material.BARRIER);
	// 		blockLocation.add(block);
	// 		if(i==3){
	// 			Location loc =getLutil().getRelativ(location, getBlockFace(), -1D, 0D);
	// 			Block blocks = loc.getBlock();
	// 			blocks.setType(Material.BARRIER);
	// 			blockLocation.add(blocks);
	// 		}
	// 	}
	// 	getObjID().addBlock(blockLocation);
	// }
	
	@EventHandler
	private void onBlockBreak(FurnitureBlockBreakEvent e){
		if(e.getID() == null || getObjID() == null) return;
		if(!e.getID().equals(getObjID())){return;}
		if(getObjID().getSQLAction().equals(SQLAction.REMOVE)){return;}
		if(!e.canBuild()){return;}
		FurnitureLib.getInstance().getLightManager().removeLight(light);
		e.remove();
		delete();
	}
	
	@EventHandler
	private void onBlockClick(FurnitureBlockClickEvent e){
		if(e.getID() == null || getObjID() == null) return;
		if(!e.getID().equals(getObjID())){return;}
		if(getObjID().getSQLAction().equals(SQLAction.REMOVE)){return;}
		if(!e.canBuild()){return;}
		if(isOn()){
			setLight(false);
		}else{
			setLight(true);
		}
	}

	@EventHandler
	private void onBlockPowered(BlockRedstoneEvent e){
		if(getObjID()==null){return;} 
		if(getObjID().getSQLAction().equals(SQLAction.REMOVE)){return;}
		if(e.getBlock()==null){return;}
		Vector loc = e.getBlock().getLocation().toVector();
		if(loc2.distance(loc)<=1){
			if(e.getNewCurrent()==0){
				setLight(false);
				redstone = false;
			}else{
				setLight(true);
				redstone = true;
			}
			return;
		}
		if(loc3.distance(loc)<=1){
			if(e.getNewCurrent()==0){
				setLight(false);
				redstone = false;
			}else{
				setLight(true);
				redstone = true;
			}
			return;
		}
	}
	
	private void setLight(Boolean b){
		if(!b){
			fEntity packet = getPacket();
			if(packet==null) return;
			packet.getInventory().setHelmet(new ItemStack(Material.REDSTONE_TORCH));
			getManager().updateFurniture(getObjID());
			FurnitureLib.getInstance().getLightManager().removeLight(light);
			return;
		}else{
			fEntity packet = getPacket();
			if(packet==null) return;
			packet.getInventory().setHelmet(new ItemStack(Material.GLOWSTONE));
			getManager().updateFurniture(getObjID());
			FurnitureLib.getInstance().getLightManager().addLight(light, 15);
			return;
		}
	}
	
	private fEntity getPacket(){
		for(fEntity packet : getManager().getfArmorStandByObjectID(getObjID())){
			if(packet.getName().equalsIgnoreCase("#LAMP#")){
				return packet;
			}
		}
		return null;
	}
	
	private boolean isOn(){
		for(fEntity as : getManager().getfArmorStandByObjectID(getObjID())){
			if(as.getName().equalsIgnoreCase("#LAMP#")){
				switch (as.getInventory().getHelmet().getType()) {
				case REDSTONE_TORCH: return false;
				case GLOWSTONE: return true;
				default: return false;
				}
			}
		}
		return false;
	}
	
	@EventHandler
	public void onFurnitureBreak(FurnitureBreakEvent e) {
		if(e.getID() == null || getObjID() == null) return;
		if(!e.getID().equals(getObjID())){return;}
		if(getObjID().getSQLAction().equals(SQLAction.REMOVE)){return;}
		if(!e.canBuild()){return;}
		FurnitureLib.getInstance().getLightManager().removeLight(light);
		e.remove();
		delete();
	}
	
	@EventHandler
	public void onFurnitureClick(FurnitureClickEvent e) {
		if(e.getID() == null || getObjID() == null) return;
		if(getObjID().getSQLAction().equals(SQLAction.REMOVE)){return;}
		if(!e.getID().equals(getObjID())){return;}
		if(!e.canBuild()){return;}
		Boolean isOn = isOn();
		fEntity packet = getPacket();
		if(packet==null) return;
		if(redstone) return;
		if(isOn){
			setLight(false);
		}else{
			setLight(true);
		}
	}
}
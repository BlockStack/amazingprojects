package com.gitlab.blockstack.BlockStackFurniture;

import com.gitlab.blockstack.BlockStackFurniture.commands.AdminCommandHandler;
import com.gitlab.blockstack.BlockStackFurniture.commands.CommandHandler;
import com.gitlab.blockstack.BlockStackFurniture.listeners.CRusticWalllampLarge;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import de.Ste3et_C0st.FurnitureLib.Crafting.Project;
import de.Ste3et_C0st.FurnitureLib.Events.FurnitureLateSpawnEvent;
import de.Ste3et_C0st.FurnitureLib.main.FurnitureLib;
import de.Ste3et_C0st.FurnitureLib.main.ObjectID;
import de.Ste3et_C0st.FurnitureLib.main.Type.CenterType;
import de.Ste3et_C0st.FurnitureLib.main.Type.PlaceableSide;
import de.Ste3et_C0st.FurnitureLib.main.Type.SQLAction;

/**
 * BlockStackFurniture Plugin
 *
 * @author Cocoa
 */
public class BlockStackFurniture extends JavaPlugin implements Listener {
	private static BlockStackFurniture self;

	/**
	 * Retrieve the plugin instance.
	 * 
	 * @return The BlockStackFurniture instance.
	 */
	public static BlockStackFurniture self() {
		return self;
	}

	@Override
	public void onEnable() {
		getCommand("BlockStackFurniture").setExecutor(new CommandHandler());
		getCommand("BlockStackFurnitureadmin").setExecutor(new AdminCommandHandler());
		self = this;

		Bukkit.getPluginManager().registerEvents(this, this);
		FurnitureLib lib = (FurnitureLib) Bukkit.getPluginManager().getPlugin("FurnitureLib");

		new Project("cRusticFireplace", this, getResource("Models/Lights/cRusticFireplace.dModel")).setSize(3, 3, 1, CenterType.CENTER);
		new Project("cRusticWallLampLarge", this, getResource("Models/Lights/cRusticWallLampLarge.dModel")).setSize(1, 2, 2, CenterType.CENTER).setPlaceableSide(PlaceableSide.SIDE);

		lib.registerPluginFurnitures(this);

		for(ObjectID id : FurnitureLib.getInstance().getFurnitureManager().getObjectList()){
			if(id==null) continue;
			if(id.getProjectOBJ() == null) continue;
			if(id.getSQLAction().equals(SQLAction.REMOVE)) continue;
			switch (id.getProjectOBJ().getName()) {
			case "cRusticWallLampLarge":new CRusticWalllampLarge(id);break;
			default:break;
			}
		  }
		this.getLogger().info("BlockStackFurniture enabled.");
	}

	@EventHandler
	public void onFurnitureLateSpawn(FurnitureLateSpawnEvent event) {
		// Hook the Furniture to the class then it will be placed
		if (event.getProject() == null)
			return;
		if (event.getProject().getName() == null)
			return;
		if (event.getID().getSQLAction().equals(SQLAction.REMOVE))
			return;
		switch (event.getProject().getName()) {
		case "cRusticWallLampLarge":
			new CRusticWalllampLarge(event.getID());
			break;
		default:
			break;
		}
	}

	@Override
	public void onDisable() {
		self = null;
		this.getLogger().info("{BlockStackFurniture} disabled.");
	}

	/**
	 * Retrieve primed gson instance.
	 * 
	 * @param pretty Enable pretty print.
	 * @return GSON instance.
	 */
	public static Gson getGson(boolean pretty) {
		GsonBuilder builder = new GsonBuilder();
		if (pretty) {
			builder.setPrettyPrinting();
		}
		return builder.create();
	}
}

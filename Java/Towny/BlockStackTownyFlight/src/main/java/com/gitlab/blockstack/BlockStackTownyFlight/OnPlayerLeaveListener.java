package com.gitlab.blockstack.BlockStackTownyFlight;

import java.text.MessageFormat;

import com.palmergames.bukkit.towny.event.PlayerLeaveTownEvent;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;


public class OnPlayerLeaveListener implements Listener {
    public static String BSChatFormat = org.bukkit.ChatColor.WHITE + "[" + org.bukkit.ChatColor.GOLD + "BSTownyFlight" + org.bukkit.ChatColor.WHITE + "]: " + org.bukkit.ChatColor.GRAY + "{0}" +  org.bukkit.ChatColor.RESET;
   // when player leaves a town
   @EventHandler
   public void onPlayerLeaveTownEvent(PlayerLeaveTownEvent event) {
       Player playername = event.getPlayer();

       // if player is op set flight to true
       if(playername.isOp()) {
           playername.setAllowFlight(true);
           playername.sendMessage(MessageFormat.format(BSChatFormat, "OP-Flight activated."));
           return;
       }

       // if player is spectator do nothing
       if(playername.getGameMode() == GameMode.SPECTATOR) {
           playername.sendMessage(MessageFormat.format(BSChatFormat, "Spectator-Flight activated."));
           return;
       }

       // if player is user turn off flight
       if(playername.getAllowFlight()) {
            playername.setFallDistance(-100000);
            playername.setAllowFlight(false);
            playername.sendMessage(MessageFormat.format(BSChatFormat,"Flight deactivated. Return to your town to fly, again."));
       }
   }
}
package com.gitlab.blockstack.BlockStackTownyFlight;

import java.text.MessageFormat;

import com.palmergames.bukkit.towny.event.PlayerEnterTownEvent;
import com.palmergames.bukkit.towny.object.Town;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class OnEnterTownListener implements Listener {
    // Chat format
    public static String BSChatFormat = org.bukkit.ChatColor.WHITE + "[" + org.bukkit.ChatColor.GOLD + "TownFlight"
            + org.bukkit.ChatColor.WHITE + "]: " + org.bukkit.ChatColor.GRAY + "{0}" + org.bukkit.ChatColor.RESET;

    // when player enters a town
    @EventHandler
    public void onEnterTownListener(PlayerEnterTownEvent event) {
        // resident should be the name of the player if they are in the town
        Player playername = event.getPlayer();
        // the name of the town which was entered
        Town townname = event.getEnteredtown();
        Bukkit.getScheduler().runTaskLater(BlockStackTownyFlight.self, () -> {
            boolean enabledFlight = CheckFlight.checkFlight(playername, townname);
            if (enabledFlight) {
                playername.sendMessage(MessageFormat.format(BSChatFormat, "Flight activated."));
            }
        }, 1);

    }
}
package com.gitlab.blockstack.BlockStackTownyFlight;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Nation;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class CheckFlight {
    public static String BSChatFormat = org.bukkit.ChatColor.WHITE + "[" + org.bukkit.ChatColor.GOLD + "BSTownyFlight"
            + org.bukkit.ChatColor.WHITE + "]: " + org.bukkit.ChatColor.GRAY + "{0}" + org.bukkit.ChatColor.RESET;

    // method checks for player's ability to fly
    public static boolean checkFlight(Player playername, Town townname) {
        // try to get nation of player; if no nation - return, if nation has resident
        // player, set flight true
        try {
            // the name of the player resident 
            Resident resident = TownyUniverse.getDataSource().getResident(playername.getName());
            // Town townname = TownyUniverse.getTownBlock(playername.getLocation()).getTown();
            // if player is Op set to allow flight
            if(playername.isOp()) {
                playername.setAllowFlight(true);
                return true;
            }

            // if player is spectator do nothing
            if(playername.getGameMode() == GameMode.SPECTATOR) {
                return false;
            }

            // if town has resident player set flight to true
            if(resident.getTown().equals(townname)) {
                playername.setAllowFlight(true);
                return true;
            }

            if (resident.getTown().getNation() == null || resident.getTown().getNation().getName().equals("")) {
                // return if null or empty
                return false;
            }

        } catch (NotRegisteredException e) {
            // no nation no resident
            return false;
        }

        // null player resident
 //       if (resident == null) {
 //           return;
 //       }

        //if town has nation get nation, if player has nation, get nation, are the nations allies; set flight if true
        try {
            // the name of the player resident 
            Resident resident = TownyUniverse.getDataSource().getResident(playername.getName());
            // name of the player's town
            Town playerTown = resident.getTown();
            if (playerTown.equals(townname)) {
                playername.setAllowFlight(true);
                return true;
            } else if (playerTown.hasNation() && townname.hasNation()) {
                Nation playerNation = playerTown.getNation();
                Nation targetNation = townname.getNation();
                if (playerNation.equals(targetNation) || playerNation.hasAlly(targetNation)) {
                    playername.setAllowFlight(true);
                    return true;
                }
            }
        } catch (Exception ex) {
            // no nation no ally
            return false;
        }
        return false;
    }
}
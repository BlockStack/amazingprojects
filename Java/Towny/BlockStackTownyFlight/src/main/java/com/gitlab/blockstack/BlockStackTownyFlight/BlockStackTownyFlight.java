package com.gitlab.blockstack.BlockStackTownyFlight;

import org.bukkit.plugin.java.JavaPlugin;

public class BlockStackTownyFlight extends JavaPlugin {

    public static BlockStackTownyFlight self;
    // fired when plugin is first enabled
    @Override
    public void onEnable() {
        this.getLogger().info("BlockStackTownyFlight is enabling...");
        getServer().getPluginManager().registerEvents(new OnJoinListener(), this);
        getServer().getPluginManager().registerEvents(new OnPlayerLeaveListener(), this);
        getServer().getPluginManager().registerEvents(new OnEnterTownListener(), this);
        self = this;
    }
    // fired when plugin is disabled
    @Override
    public void onDisable() {
        this.getLogger().info("BlockStackTownyFlight is disabling...");
    }
}
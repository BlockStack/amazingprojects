package com.gitlab.blockstack.BlockStackTownyFlight;

import java.text.MessageFormat;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.WorldCoord;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class OnJoinListener implements Listener {
    // chat format
    public static String BSChatFormat = org.bukkit.ChatColor.WHITE + "[" + org.bukkit.ChatColor.GOLD + "BSTownyFlight" + org.bukkit.ChatColor.WHITE + "]: " + org.bukkit.ChatColor.GRAY + "{0}" +  org.bukkit.ChatColor.RESET;
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player playername = event.getPlayer();
        // Town townname;
        
        // if player is op set flight to true
        if(playername.isOp()) {
            playername.setAllowFlight(true);
            playername.sendMessage(MessageFormat.format(BSChatFormat, "OP-Flight activated."));
            return;
        }

        // if player is spectator do nothing
        if(playername.getGameMode() == GameMode.SPECTATOR) {
            playername.sendMessage(MessageFormat.format(BSChatFormat, "Spectator-Flight activated."));
            return;
        }

        // run task later to account for login lag
        Bukkit.getScheduler().runTaskLater(BlockStackTownyFlight.self, () -> {
            // try to set flight if player is inside town, nation, or nation ally, of which they are a resident
            try {
                Town townname = WorldCoord.parseWorldCoord(playername.getLocation()).getTownBlock().getTown();
                boolean flightOn = CheckFlight.checkFlight(playername, townname);
                if (flightOn) {
                    playername.sendMessage(MessageFormat.format(BSChatFormat, "Flight activated."));
                }
            } catch (NotRegisteredException e) {
                playername.sendMessage(MessageFormat.format(BSChatFormat,"Flight deactivated. Return to your town to fly, again."));
                return;
            }
        }, 40);
    }
}